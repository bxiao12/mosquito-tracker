package com.example.mosquitotracker

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class TipsAdapter(private val fragment: TipsFragment, private val dataSet: Array<String>) :
    RecyclerView.Adapter<TipsAdapter.TipsViewHolder>() {

    class TipsViewHolder(view: View, fragment: TipsFragment) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        var textView = view.findViewById<TextView>(R.id.itemText)
        var expandedFragment = fragment

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            expandedFragment.expandListItem(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TipsViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.tips_item_view, parent, false)
        return TipsViewHolder(layout, fragment)
    }

    override fun onBindViewHolder(holder: TipsViewHolder, position: Int) {
        holder.textView.text = dataSet[position]
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }
}