package com.example.mosquitotracker

import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class DatabaseInterface {
    var present = ArrayList<Boolean>()

    data class Coor(
        var lat: Double = 0.0,
        var longi: Double = 0.0,
        val c: Float = 0.0f
    )

    /**
     * Stores a location into the database as a Coor object, storing its latitude and longitude
     * @param lat the latitude of a given location
     * @param longi the longitude of a given location
     */
    fun reportActivity(lat: Double, longi: Double) {
        reportActivity(lat, longi, 0.0f)
    }

    fun reportActivity(lat: Double, longi: Double, c: Float) {
        val db = FirebaseDatabase.getInstance()
        val loc = db.getReference("locations")

        var coors = Coor(lat, longi, c)
        var pkey = loc.push().key
        loc.child(pkey!!).setValue(coors)
    }

    /**
     * Checks to see if a location has been reported for mosquitos. As this is a long operation, it
     * will return an arraylist where element 0 is the value and element 1 is the completed boolean
     * @param lat the latitude of a given location
     * @param longi the longitude of a given location
     */
    fun checkForActivity(lat: Double, longi: Double): ArrayList<Boolean> {
        present.add(0, false)
        present.add(1, false)

        val db = FirebaseDatabase.getInstance().getReference("locations")

        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //iterating through all the nodes
                for (loc in dataSnapshot.children) {
                    var coors = loc.getValue<Coor>(Coor::class.java)

                    if (coors!!.lat == lat && coors!!.longi == longi) {
                        present[0] = true
                        present[1] = true
                        break
                    }
                }
            }

            //only here because kotlin made me put it here
            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

        return present
    }

    /**
     * This returns a list of all of the reported coordinates, stored as a Coor data type
     */
    fun getListOfCoordinates(caller: MapFragment, prevList: ArrayList<LatLng>): ArrayList<LatLng> {
        var coorList = ArrayList<LatLng>()

        val db = FirebaseDatabase.getInstance().getReference("locations")

        db.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //iterating through all the nodes
                for (loc in dataSnapshot.children) {

                    var coors = loc.getValue<Coor>(Coor::class.java)!!
                    var ll = LatLng(coors.lat, coors.longi)

                    //check if already in list
                    if (!prevList.contains(ll)) {
                        caller.highlightActivity(ll)
                        coorList.add(ll)
                    }
                }
            }

            //only here because kotlin made me put it here
            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

        return coorList
    }

    /**
     * Allows deletion of a stored location in the database
     */
    fun deleteLoc(lat: Double, longi: Double): Boolean {
        var coorList = false

        val db = FirebaseDatabase.getInstance().getReference("locations")

        db.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                //iterating through all the nodes
                for (loc in dataSnapshot.children) {
                    var coors = loc.getValue<Coor>(Coor::class.java)

                    if (coors!!.lat == lat && coors!!.longi == longi) {
                        val elem = FirebaseDatabase.getInstance().getReference("locations")
                            .child(loc.key!!)
                        elem.removeValue()
                    }
                }
            }

            //only here because kotlin made me put it here
            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

        return coorList
    }

}