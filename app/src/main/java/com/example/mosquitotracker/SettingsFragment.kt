package com.example.mosquitotracker

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.provider.Settings
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat


class SettingsFragment : PreferenceFragmentCompat() {
    private lateinit var notifications: SwitchPreferenceCompat
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private lateinit var locationSettings: Preference
    private lateinit var permissions: Preference
    private lateinit var feedback: Preference
    private lateinit var about: Preference

    // Set for the frequency of mosquito activity, 86400000 is 24 hours.
    private var notificationTime: Long = 86400000L

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings)
        val mainActivity = activity as MainActivity
        sharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        notifications = findPreference(NOTIFICATIONS_KEY) as SwitchPreferenceCompat
        locationSettings = findPreference(LOCATION_SETTINGS_KEY)
        permissions = findPreference(PERMISIONS_KEY)
        feedback = findPreference(FEEDBACK_KEY)
        about = findPreference(ABOUT_KEY)

        notifications.setOnPreferenceChangeListener { _, _ ->
            var mAlarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val wInt = Intent(
                context,
                WeatherReceiver::class.java
            )

            // Create intent to see it if exists
            var wri = PendingIntent.getBroadcast(
                context, 0, wInt,
                PendingIntent.FLAG_NO_CREATE
            )

            if (!notifications.isChecked && wri == null) {
                wri = PendingIntent.getBroadcast(
                    context, 0, wInt,
                    0
                )
                // Set repeating alarm
                mAlarmManager.setRepeating(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    notificationTime,
                    wri
                )

                notifications.isChecked = true
                Log.i("noti", "Notifications enabled")
            } else if (notifications.isChecked && wri != null) {
                mAlarmManager.cancel(wri)
                wri.cancel()
                notifications.isChecked = false
                Log.i("noti", "Notifications disabled")
            }

            true
        }
        locationSettings.setOnPreferenceClickListener {
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
            true
        }
        permissions.setOnPreferenceClickListener {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri: Uri = Uri.fromParts("package", activity!!.packageName, null)
            intent.data = uri
            startActivity(intent)
            true
        }
        feedback.setOnPreferenceClickListener {
            fragmentManager!!.beginTransaction()
                .setCustomAnimations(
                    R.anim.slide_in_up,
                    R.anim.slide_out_up,
                    R.anim.slide_out_down,
                    R.anim.slide_in_down
                )
                .replace(R.id.fragment_content, FeedbackFragment())
                .addToBackStack(null)
                .commit()
            mainActivity.setBottomBarGone()
            true
        }
        about.setOnPreferenceClickListener {
            fragmentManager!!.beginTransaction()
                .setCustomAnimations(
                    R.anim.slide_in_up,
                    R.anim.slide_out_up,
                    R.anim.slide_out_down,
                    R.anim.slide_in_down
                )
                .replace(R.id.fragment_content, AboutFragment())
                .addToBackStack(null)
                .commit()
            mainActivity.setBottomBarGone()
            true
        }
    }

    override fun onResume() {
        super.onResume()

        // Sets the push notifications selection state from SharedPreferences
        if (NotificationManagerCompat.from(context!!).areNotificationsEnabled()) {
            notifications.isChecked = sharedPreferences.getBoolean(NOTIFICATIONS_KEY, true)
            notifications.isEnabled = true
        } else {
            notifications.isChecked = false
            notifications.isEnabled = false
        }
    }

    override fun onPause() {
        super.onPause()

        // Saves the push notifications selection state to SharedPreferences
        editor.putBoolean(NOTIFICATIONS_KEY, notifications.isChecked)
        editor.commit()
    }


    companion object {
        private const val NOTIFICATIONS_KEY = "notifications"
        private const val LOCATION_SETTINGS_KEY = "location_settings"
        private const val PERMISIONS_KEY = "permissions"
        private const val FEEDBACK_KEY = "feedback"
        private const val ABOUT_KEY = "about"
    }

}