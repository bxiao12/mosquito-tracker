package com.example.mosquitotracker

import android.content.res.Resources
import android.text.SpannableString
import android.text.style.BulletSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListTextAdapter(
    private val listTextKeys: List<Int>,
    private val listTextValues: List<Int>,
    private val resources: Resources
) :
    RecyclerView.Adapter<ListTextAdapter.ListTextViewHolder>() {
    class ListTextViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var subtitleTextView = view.findViewById<TextView>(R.id.subtitle)
        var listTextView = view.findViewById<TextView>(R.id.list_text)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTextViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_text_view, parent, false)
        return ListTextViewHolder(layout)
    }

    override fun onBindViewHolder(holder: ListTextViewHolder, position: Int) {
        if (listTextKeys[position] != -1) {
            holder.subtitleTextView.visibility = View.VISIBLE
            holder.subtitleTextView.text = resources.getString(listTextKeys[position])
        }
        holder.listTextView.text =
            resources.getStringArray(listTextValues[position]).toBulletedList()
    }

    override fun getItemCount(): Int {
        return listTextKeys.size
    }

    private fun Array<String>.toBulletedList(): CharSequence {
        return SpannableString(this.joinToString("\n")).apply {
            this@toBulletedList.foldIndexed(0) { index, acc, span ->
                val end = acc + span.length + if (index != this@toBulletedList.size - 1) 1 else 0
                this.setSpan(BulletSpan(18), acc, end, 0)
                end
            }
        }
    }
}