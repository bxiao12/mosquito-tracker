package com.example.mosquitotracker

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.google.android.material.button.MaterialButton


class FeedbackFragment : Fragment() {
    private lateinit var closeButton: ImageView
    private lateinit var sendButton: MaterialButton
    private lateinit var editText: EditText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.feedback_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mainActivity = activity as MainActivity
        closeButton = view.findViewById(R.id.close_button_feedback)
        sendButton = view.findViewById(R.id.send_button)
        editText = view.findViewById(R.id.feedback_edit)
        closeButton.setOnClickListener {

            // Closes the keyboard if displayed on users screen
            val imm =
                activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
            mainActivity.closeSettingsFragment()
        }
        sendButton.setOnClickListener {
            if (editText.text.isNotEmpty()) {

                // Sends an email to our feeback email set up with gmail
                val emailIntent = Intent(
                    Intent.ACTION_SENDTO,
                    Uri.parse("mailto:" + getString(R.string.feedback_email))
                )
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
                emailIntent.putExtra(Intent.EXTRA_TEXT, editText.text.toString())
                startActivity(emailIntent)
                editText.setText("")
            }
        }
    }
}