package com.example.mosquitotracker


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMapClickListener,
    View.OnClickListener, GoogleMap.OnMarkerClickListener {

    private var numMarkersNowSelected = 0
    private var undoFabIsVisible = false
    private var showFabs = false
    private var dialogShowed = false
    private var locSettingsDialogShowed = false
    private var followingUser = true
    private var appJustStarted = true
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var userLocation: Location? = null
    private lateinit var mMap: GoogleMap
    private lateinit var locationFab: FloatingActionButton
    private lateinit var markMosquitoFab: FloatingActionButton
    private lateinit var undoFab: FloatingActionButton
    private lateinit var optionsFab: FloatingActionButton
    private lateinit var primaryColor: ColorFilter
    private lateinit var primaryColorDark: ColorFilter
    private var circleList: MutableList<Triple<Marker, Circle, Int>> = mutableListOf()
    private var isMarkingMosquitoActivity = false
    private var isOpen: Boolean = false
    private lateinit var fabOpen: Animation
    private lateinit var fabClose: Animation
    private lateinit var fabClock: Animation
    private lateinit var fabAntiClock: Animation
    private var reportedLocations = ArrayList<LatLng>()
    private var updateWeather = true

    //Animations for fabs
    private fun initAnimations() {
        fabOpen = AnimationUtils.loadAnimation(context, R.anim.fab_open)
        fabClose = AnimationUtils.loadAnimation(context, R.anim.fab_close)
        fabClock = AnimationUtils.loadAnimation(context, R.anim.fab_rotate_clock)
        fabAntiClock = AnimationUtils.loadAnimation(context, R.anim.fab_rotate_anticlock)
    }

    //Setting up buttons, their functionality, and colors.
    private fun initButtonControls() {
        primaryColor = PorterDuffColorFilter(
            ContextCompat.getColor(context!!, R.color.colorPrimary),
            PorterDuff.Mode.SRC_IN
        )
        primaryColorDark = PorterDuffColorFilter(
            ContextCompat.getColor(context!!, R.color.colorPrimaryDark),
            PorterDuff.Mode.SRC_IN
        )
        optionsFab = view!!.findViewById(R.id.options_fab)
        markMosquitoFab = view!!.findViewById(R.id.mark_mosquito_fab)
        markMosquitoFab.setOnClickListener(this)
        undoFab = view!!.findViewById(R.id.undo_mark_fab)
        //Delete fab
        undoFab.setOnClickListener {
            numMarkersNowSelected = 0
            var removedMarkerCount = 0
            isMarkingMosquitoActivity = false
            markMosquitoFab.background.colorFilter = primaryColor
            //Go through list of markers and collect those selected for deletion
            var tripleList: MutableList<Triple<Marker, Circle, Int>> = mutableListOf()
            for (tripleTemp in circleList)
                if (tripleTemp.third == 1) {
                    tripleList.add(tripleTemp)
                    numMarkersNowSelected--
                }
            //Go through list of markers being deleted
            for (tripleTemp in tripleList) {
                //delete from database
                var p0 = tripleTemp.first
                val ll = p0.position
                val db = DatabaseInterface()
                db.deleteLoc(ll.latitude, ll.longitude)
                reportedLocations.remove(ll)
                //Delete from view
                circleList.remove(tripleTemp)
                tripleTemp.first.isVisible = false
                tripleTemp.first.remove()
                tripleTemp.second.isVisible = false
                tripleTemp.second.remove()
                undoFab.background.colorFilter = primaryColor
                removedMarkerCount++
            }
            undoFab.startAnimation(fabClose)
            undoFab.isClickable = false
            undoFabIsVisible = false
            //Toasts to let user know marker(s) were removed
            if (removedMarkerCount == 1) {
                Toast.makeText(context, MARKER_REMOVED, Toast.LENGTH_SHORT).show()
            } else if (removedMarkerCount > 1) {
                Toast.makeText(context, MARKERS_REMOVED, Toast.LENGTH_SHORT).show()
            }
        }
        locationFab = view!!.findViewById(R.id.location_fab)
        //Setup location fab to center on current user location
        locationFab.setOnClickListener {
            isMarkingMosquitoActivity = false
            markMosquitoFab.background.colorFilter = primaryColor
            if (userLocation != null) {
                followingUser = true
                val userLatLong = LatLng(userLocation!!.latitude, userLocation!!.longitude)
                if (mMap.cameraPosition.zoom > 11)
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(userLatLong))
                else
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLong, 11.0f))

            } else {
                Log.e("UserLocationError", "User Location does not exist")
                val builder = AlertDialog.Builder(context!!)
                builder.setTitle("App Functionality")
                builder.setMessage("In order to use this app effectively, please enable location services in your device's settings.")
                builder.setPositiveButton("Go to Settings") { _, _ ->
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                builder.setNegativeButton("Cancel") { _, _ -> }
                var dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }
        //Setup options fab to show marker placer fab (and delete fab is markers are selected)
        optionsFab.setOnClickListener {
            if (isOpen) {
                markMosquitoFab.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.fab_close
                    )
                )
                optionsFab.startAnimation(fabAntiClock)
                if (undoFabIsVisible)
                    undoFab.startAnimation(fabClose)
                markMosquitoFab.isClickable = false
                undoFab.isClickable = false
                undoFabIsVisible = false
                isOpen = false
                unselectAllMarkers()
            } else {
                markMosquitoFab.startAnimation(fabOpen)
                optionsFab.startAnimation(fabClock)
                markMosquitoFab.isClickable = true
                isOpen = true
                if (numMarkersNowSelected > 0) {
                    undoFab.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fab_open))
                    undoFab.isClickable = true
                    undoFabIsVisible = true
                }
            }
        }
        markMosquitoFab.hide()
        optionsFab.hide()
        undoFab.hide()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        //Allow weather updates
        updateWeather = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Setup user tracking and centering
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                userLocation = p0.lastLocation
                Log.i(
                    "oncreate, userlocation (lat,lng) is: ",
                    userLocation!!.latitude.toString() + " " + userLocation!!.longitude.toString()
                )

                if (appJustStarted) {
                    appJustStarted = false
                    if (!mMap.isMyLocationEnabled)
                        mMap.isMyLocationEnabled = true
                    mMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                userLocation!!.latitude,
                                userLocation!!.longitude
                            ), 11.0f
                        )
                    )
                } else if (followingUser) {
                    mMap.moveCamera(
                        CameraUpdateFactory.newLatLng(
                            LatLng(
                                userLocation!!.latitude,
                                userLocation!!.longitude
                            )
                        )
                    )
                }
            }
        }
        createLocationRequest()
    }

    //setting the reported mosquito locations
    override fun onResume() {
        super.onResume()
        //Check and/or request permissions.
        if (ActivityCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
            && canGetLocation()
        ) {
            //Update locations marked with mosquito activity from firebase
            var db = DatabaseInterface()
            reportedLocations = db.getListOfCoordinates(this, reportedLocations)
            if (!locationUpdateState) {
                startLocationUpdates()
            }
            if (showFabs) {
                optionsFab.show()

            }
        } else if (!locationUpdateState) {
            startLocationUpdates()
        }
    }

    //Response for requesting location information
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdates()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    //Creating mapview
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.map_fragment, container, false
        )
    }

    //initialize support map fragment, fused location client for user location information, buttons, and animations.
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val fragmentManager = childFragmentManager
        val mapFragment = fragmentManager
            .findFragmentById(R.id.map_fragment) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        initAnimations()
        initButtonControls()
    }


    //Enable map gestures and button onclick listeners
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomGesturesEnabled = true
        mMap.uiSettings.isRotateGesturesEnabled = true
        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isMyLocationButtonEnabled = false
        mMap.setOnMapClickListener(this)
        mMap.setOnMarkerClickListener(this)
        mMap.setOnCameraMoveListener { followingUser = false }
        setUpMap()
    }

    //Setting up map with permissions and listening for user updated positions
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { loc ->
            if (loc != null) {
                Log.i("Location", loc.toString())
                val userLatLong = LatLng(loc!!.latitude, loc!!.longitude)
                if (updateWeather) {
                    //var wi = WeatherHandler(userLatLong, context!!)
                    //wi.getWeatherInfo()
                    updateWeather = false
                }
            }
        }
    }

    //Drawing circle at location indicated by user
    private fun drawCircle(p0: LatLng?, markerColor: Float) {
        val circle = mMap.addCircle(
            CircleOptions().center(p0).radius(50.0).strokeColor(R.color.transparentRed).fillColor(R.color.transparentRed)
        )
        val marker = mMap.addMarker(
            MarkerOptions().position(p0!!).icon(
                BitmapDescriptorFactory.defaultMarker(markerColor)
            )
        )
        var isSelectedbyUser = 0
        if (markerColor == 215f) //Means user clicked on marker, should add as marked by user
            isSelectedbyUser = 1;
        circleList.add(Triple(marker, circle, isSelectedbyUser))
    }

    //Highlight markers selected by user
    override fun onMarkerClick(p0: Marker?): Boolean {
        isMarkingMosquitoActivity = false
        markMosquitoFab.background.colorFilter = primaryColor
        numMarkersNowSelected = 0;
        lateinit var triple: Triple<Marker, Circle, Int>
        var markerColor = 200f
        //Keep track of which markers are selected for the case where user click delete fab
        for (tripleTemp in circleList) {
            if (!tripleTemp.first.equals(p0) && tripleTemp.third == 1) {
                numMarkersNowSelected++
            } else if (tripleTemp.first.equals(p0) && tripleTemp.third == 0) {
                triple = tripleTemp
                markerColor = 215f
                numMarkersNowSelected++
            } else if (tripleTemp.first.equals(p0) && tripleTemp.third == 1) {
                triple = tripleTemp
            }
        }
        //Draw new marker on map with different color
        drawCircle(p0!!.position, markerColor)
        //Remove old marker now that new marker is in place
        circleList.remove(triple)
        triple.first.isVisible = false
        triple.first.remove()
        triple.second.isVisible = false
        triple.second.remove()
        //Depending on number of markers selected, show or don't show the various fabs
        if (numMarkersNowSelected > 0) {
            if (!isOpen) {
                isOpen = true
                markMosquitoFab.startAnimation(fabOpen)
                optionsFab.startAnimation(fabClock)
                markMosquitoFab.isClickable = true
            }
            if (!undoFabIsVisible) {
                undoFab.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fab_open))
                undoFab.isClickable = true
                undoFabIsVisible = true
            }

        } else {
            if (isOpen) {
                if (undoFabIsVisible) {
                    undoFab.startAnimation(fabClose)
                    undoFab.isClickable = false
                    undoFabIsVisible = false
                }
            }
        }
        return true
    }

    fun highlightActivity(p0: LatLng) {
        drawCircle(p0, 200f)
        markMosquitoFab.background.colorFilter = primaryColor
    }

    //Allow users to mark map with areas of interest
    override fun onMapClick(p0: LatLng?) {
        if (isMarkingMosquitoActivity) {
            drawCircle(p0, 200f)
            //add to the database
            val dbInt = DatabaseInterface()
            dbInt.reportActivity(p0!!.latitude, p0.longitude)
            reportedLocations.add(p0)
        } else
            followingUser = false
    }

    //Keep track of if user has clicked fab for marking map
    override fun onClick(v: View?) {
        if (!isMarkingMosquitoActivity) {
            isMarkingMosquitoActivity = true
            markMosquitoFab.background.colorFilter = primaryColorDark
        } else {
            isMarkingMosquitoActivity = false
            markMosquitoFab.background.colorFilter = primaryColor
        }
    }

    private fun canGetLocation(): Boolean {
        return isLocationEnabled(context!!); // application context
    }

    //Check location settings
    private fun isLocationEnabled(context: Context): Boolean {
        var locationMode = 0
        var locationProviders = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            locationMode =
                Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE)
            return locationMode != Settings.Secure.LOCATION_MODE_OFF
        } else {
            locationProviders = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.LOCATION_MODE
            );
            return !TextUtils.isEmpty(locationProviders)
        }
    }

    //Check location permissions and notify user if unable to access location effectively. Give option to go to settings to change their permissions
    private fun startLocationUpdates() {
        Log.i("start locationupdates reached", "test")
        if (!canGetLocation()) {
            if (!locSettingsDialogShowed) {
                locSettingsDialogShowed = true
                val builder = AlertDialog.Builder(context!!)
                builder.setTitle("App Functionality")
                builder.setMessage("In order to use this app effectively, please enable location services in your device's settings.")
                builder.setPositiveButton("Go to Settings") { _, _ ->
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                builder.setNegativeButton("Cancel") { _, _ -> }
                var dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }

        if (ActivityCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity!!,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                if (!dialogShowed) {
                    dialogShowed = true
                    val builder = AlertDialog.Builder(context!!)
                    builder.setTitle("App Functionality")
                    builder.setMessage("In order to use this app effectively, please allow access to your current location.")
                    builder.setPositiveButton("Ok") { _, _ ->

                        markMosquitoFab.hide()
                        undoFab.hide()
                        undoFabIsVisible = false
                        optionsFab.hide()
                        ActivityCompat.requestPermissions(
                            activity!!,
                            arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                            LOCATION_PERMISSION_REQUEST_CODE
                        )
                    }
                    var dialog: AlertDialog = builder.create()
                    dialog.show()
                }

            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        } else
            showFabs = true
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    //Location requests for user location every set time interval
    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 50
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        val client = LocationServices.getSettingsClient(activity!!)
        val task = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
    }

    //Unselect all markers currently selected by user and reflect in selected list of markers
    private fun unselectAllMarkers() {
        var markerColor = 200f
        var tripleList: MutableList<Triple<Marker, Circle, Int>> = mutableListOf()
        for (tripleTemp in circleList) {
            if (tripleTemp.third == 1) {
                //
                tripleList.add(tripleTemp)
            }
        }
        for (tripleTemp in tripleList) {
            drawCircle(tripleTemp.first.position, markerColor)
            circleList.remove(tripleTemp)
            tripleTemp.first.isVisible = false
            tripleTemp.first.remove()
            tripleTemp.second.isVisible = false
            tripleTemp.second.remove()
            //undoFab.background.colorFilter = primaryColor
        }
        numMarkersNowSelected = 0

    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val MARKER_REMOVED = "Marker Removed"
        private const val MARKERS_REMOVED = "Markers Removed"
        private const val REQUEST_CHECK_SETTINGS = 2
    }
}
