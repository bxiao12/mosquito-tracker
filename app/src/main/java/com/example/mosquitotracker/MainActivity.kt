package com.example.mosquitotracker

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var fragmentManager: FragmentManager
    private var prevFrag: Int = 0
    private var currentFrag: Int = 0
    private lateinit var sharedPreferences: SharedPreferences
    //set for the frequency of mosquito activity, 86400000 is 24 hours.
    private var notificationTime: Long = 86400000L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragment_content, MapFragment())
        fragmentTransaction.commit()
        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.action_map
        currentFrag = R.id.action_map
        prevFrag = R.id.action_map
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            switchFragment(item.itemId)
        }

        setNotifications()
    }

    private fun setNotifications() {
        sharedPreferences = this.getPreferences(Context.MODE_PRIVATE)
        if (sharedPreferences.getBoolean("notifications", true)) {
            // Get the AlarmManager Service
            var mAlarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

            // Create an Intent to broadcast to the AlarmNotificationReceiver
            val wInt = Intent(
                this,
                WeatherReceiver::class.java
            )

            var exist = PendingIntent.getBroadcast(
                this, 0, wInt,
                PendingIntent.FLAG_NO_CREATE
            )

            if (exist == null) {
                // Create an PendingIntent that holds the NotificationReceiverIntent
                var wri = PendingIntent.getBroadcast(
                    baseContext, 0, wInt, 0
                )

                // Set repeating alarm
                mAlarmManager.setRepeating(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    notificationTime,
                    wri
                )
            }
        }
    }

    private fun switchFragment(pos: Int): Boolean {
        val fragmentTransaction = fragmentManager.beginTransaction()
        when (pos) {
            R.id.action_tips -> {
                fragmentTransaction.replace(R.id.fragment_content, TipsFragment())
                if (currentFrag == R.id.action_map) {
                    fragmentTransaction.addToBackStack(null)
                }
                prevFrag = currentFrag
                currentFrag = R.id.action_tips
            }
            R.id.action_settings -> {
                fragmentTransaction.replace(R.id.fragment_content, SettingsFragment())
                if (currentFrag == R.id.action_map) {
                    fragmentTransaction.addToBackStack(null)
                }
                prevFrag = currentFrag
                currentFrag = R.id.action_settings
            }
            else -> {
                fragmentManager.popBackStack()
                prevFrag = R.id.action_map
                currentFrag = R.id.action_map
            }
        }
        fragmentTransaction.commit()
        return true
    }

    fun closeExpandedFragment(displayedFrag: ExpandedItemFragment) {
        if (displayedFrag.isFullScreen()) {
            displayedFrag.closeFullScreen()
        } else {
            bottomNavigationView.visibility = View.VISIBLE
            fragmentManager.popBackStack()
        }
    }

    fun closeSettingsFragment() {
        bottomNavigationView.visibility = View.VISIBLE
        fragmentManager.popBackStack()
    }

    override fun onBackPressed() {
        val displayedFrag: Fragment?
        if (fragmentManager.backStackEntryCount > 0) {
            displayedFrag = fragmentManager.fragments[fragmentManager.backStackEntryCount - 1]
        } else {
            displayedFrag = null
        }

        if (displayedFrag is ExpandedItemFragment) {
            closeExpandedFragment(displayedFrag)
        } else if (displayedFrag is FeedbackFragment || displayedFrag is AboutFragment) {
            closeSettingsFragment()
        } else if (currentFrag != R.id.action_map) {
            bottomNavigationView.selectedItemId = R.id.action_map
            fragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    fun setBottomBarGone() {
        bottomNavigationView.visibility = View.GONE
    }

}
