package com.example.mosquitotracker

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.AsyncTask
import android.util.Log
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import kotlin.math.abs

class WeatherReceiver : BroadcastReceiver() {

    private lateinit var loc: LatLng

    override fun onReceive(context: Context, intent: Intent) {
        //get location
        var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        //the notification is disabled if there is no location information
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    loc = LatLng(location.latitude, location.longitude)
                    getWeatherInfo(context)
                }
            }
        /*used for debugging
        loc = (LatLng(-28.54,-81.38))
        getWeatherInfo(context)*/
    }

    fun updateLocation(newLoc: LatLng) {
        loc = newLoc
    }

    fun getWeatherInfo(cont: Context) {
        GetWeather(loc, cont).execute()
    }

    internal class GetWeather(inLoc: LatLng, inCont: Context) : AsyncTask<Void, Void, String>() {
        private val loc = inLoc
        private val cont = inCont
        private lateinit var nm: NotificationManager

        override fun doInBackground(vararg params: Void?): String {
            //initialize notification
            nm = cont.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            var sLat = (loc.latitude).toString()
            var sLon = (loc.longitude).toString()
            var conn: HttpURLConnection? = null
            var d: String? = null
            var reader: BufferedReader? = null
            val locURL = "$URL_START$sLat,$sLon"
            Log.i(TAG, locURL)

            try {
                conn = URL(locURL).openConnection() as HttpURLConnection
                val wd = BufferedInputStream(conn.inputStream)
                Log.i(TAG, "trying")
                //read data and create a string
                reader = BufferedReader(InputStreamReader(wd))
                val sep = System.getProperty("line.separator")
                val data = StringBuilder()

                //iterate through the output
                reader.forEachLine {
                    Log.i(TAG, "Reading from socket")
                    data.append(it + sep)
                }
                d = data.toString()

            } catch (exception: MalformedURLException) {
                Log.e(TAG, "Bad URL")
            } catch (exception: IOException) {
                Log.e(TAG, "Unknown IO error")
            } finally {
                conn?.disconnect()
                reader?.close()
            }
            // Parse the JSON-formatted response

            var outp = todayForcast(d)
            Log.i("forcast", outp)
            return outp
        }

        private fun todayForcast(data: String?): String {

            var result = "problem"

            try {
                // Get top-level JSON Object - a Map
                val responseObject = JSONTokener(
                    data
                ).nextValue() as JSONObject

                //find the daily weather forcast
                val today = responseObject
                    .getJSONObject("daily")

                val tf = (today.getJSONArray("data")).get(0) as JSONObject

                var temp = tf.getDouble("temperatureHigh")
                var hum = tf.getDouble("humidity")

                result = mosLikely(temp, hum)

            } catch (e: JSONException) {
                Log.e("forcast", "problem getting forecast", e)
            }
            return result
        }

        private fun mosLikely(temp: Double, hum: Double): String {
            var pHum = hum * 100
            var outMsg = "Error"
            if (temp <= 50) {
                outMsg =
                    "High today is $temp ºF; mosquitoes unlikely."
            } else if (temp >= 104) {
                outMsg =
                    "High today is $temp ºF; mosquitoes unlikely."
            } else {
                var vari = abs(80 - temp)
                if (vari <= 5) {
                    if (hum > 0.30) {
                        outMsg =
                            "High today is $temp ºF; mosquitoes likely."
                    } else {
                        outMsg =
                            "High today is $temp ºF; mosquitoes unlikely."
                    }
                } else if (vari <= 10) {
                    if (hum > 0.30) {
                        outMsg =
                            "High today is $temp ºF; mosquitoes likely."
                    } else {
                        outMsg =
                            "High today is $temp ºF; mosquitoes unlikely."
                    }
                } else if (vari <= 20) {
                    if (hum > 0.30) {
                        outMsg = "High today is $temp ºF; mosquitoes likely."
                    } else {
                        outMsg =
                            "High today is $temp ºF; mosquitoes unlikely."
                    }
                } else {
                    outMsg = "High today is $temp ºF; mosquitoes unlikely."
                }
            }

            return outMsg
        }

        override fun onPostExecute(outMsg: String) {
            //Toast.makeText(cont, outMsg, Toast.LENGTH_LONG).show()
            startNoti(outMsg)
        }

        private fun createNC() {

            val chan = NotificationChannel(
                "Mosquito Tracker",
                "Mosquito Weather Prediction", NotificationManager.IMPORTANCE_HIGH
            )

            // Configure the notification channel.
            chan.description = "This provides expected mosquito activity based on today's weather"
            chan.enableLights(true)
            chan.lightColor = Color.GREEN

            nm.createNotificationChannel(chan)
        }

        private fun startNoti(msg: String) {
            createNC()
            // Define action Intent
            val mNotificationIntent = Intent(
                cont,
                MainActivity::class.java
            ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val mContentIntent = PendingIntent.getActivity(
                cont, 0,
                mNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
            )


            // Define the Notification's expanded message and Intent:

            val notificationBuilder = Notification.Builder(cont, "Mosquito Tracker")
                .setTicker("Mosquito Activity Prediction")
                .setSmallIcon(android.R.drawable.stat_sys_warning)
                .setAutoCancel(true)
                .setContentTitle("MosquitoTracker")
                .setContentText("$msg")
                .setContentIntent(mContentIntent)

            // Pass the Notification to the NotificationManager:
            nm.notify(1, notificationBuilder.build())
        }
    }

    companion object {
        private const val URL_START =
            "https://api.darksky.net/forecast/1c092d88732c957ad4b32b44efda675e/"
        private const val TAG = "WEATHER"
    }
}