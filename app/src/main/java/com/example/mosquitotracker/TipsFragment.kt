package com.example.mosquitotracker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class TipsFragment : Fragment() {
    private lateinit var tipsRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.tips_fragment, container, false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val dataSet = createDataSet()
        initializeRecyclerView(dataSet)
    }

    private fun initializeRecyclerView(dataSet: Array<String>) {
        tipsRecyclerView = view!!.findViewById<RecyclerView>(R.id.tips_recycler_view)
        val tipsAdapter = TipsAdapter(this, dataSet)
        val linearLayoutManager = LinearLayoutManager(context!!)
        val dividerItemDecoration = DividerItemDecoration(
            tipsRecyclerView.context,
            linearLayoutManager.orientation
        )
        tipsRecyclerView.layoutManager = linearLayoutManager
        tipsRecyclerView.addItemDecoration(dividerItemDecoration)
        tipsRecyclerView.adapter = tipsAdapter
    }

    private fun createDataSet(): Array<String> {
        return arrayOf(
            getString(R.string.mosquito_program),
            getString(R.string.tips_mosquito_video),
            getString(R.string.about_mosquitoes),
            getString(R.string.asian_tiger_mosquito), getString(R.string.northern_house_mosquito),
            getString(R.string.tips_breeding), getString(R.string.examples_of_breeding_areas),
            getString(R.string.what_can_you_do)
        )
    }

    fun expandListItem(pos: Int) {
        val mainActivity = activity as MainActivity
        val expandedFragment = getExpandedItemFragment(pos)
        val fragmentTransaction = mainActivity.supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(
            R.anim.slide_in_up,
            R.anim.slide_out_up,
            R.anim.slide_out_down,
            R.anim.slide_in_down
        )
        fragmentTransaction.replace(R.id.fragment_content, expandedFragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
        mainActivity.setBottomBarGone()
    }

    private fun getExpandedItemFragment(pos: Int): ExpandedItemFragment {
        return when (pos) {
            0 -> ExpandedItemFragment(
                R.string.mosquito_program,
                arrayOf(R.mipmap.university_park_foreground),
                false,
                emptyMap(),
                R.string.mosquito_program_desc
            )
            1 -> ExpandedItemFragment(
                R.string.tips_mosquito_video,
                emptyArray(),
                true,
                emptyMap(),
                R.string.tips_mosquito_video_desc
            )
            2 -> ExpandedItemFragment(
                R.string.about_mosquitoes,
                arrayOf(R.mipmap.mosquito_swarm_foreground),
                false,
                emptyMap(),
                R.string.about_mosquitoes_desc
            )
            3 -> ExpandedItemFragment(
                R.string.asian_tiger_mosquito,
                arrayOf(R.mipmap.asian_tiger_mosquito_foreground),
                false,
                mapOf(-1 to R.array.asian_tiger_mosquito_list),
                R.string.asian_tiger_mosquito_desc
            )
            4 -> ExpandedItemFragment(
                R.string.northern_house_mosquito,
                arrayOf(R.mipmap.northern_house_mosquito_foreground),
                false,
                mapOf(-1 to R.array.northern_house_mosquito_list),
                R.string.northern_house_mosquito_desc
            )
            5 -> ExpandedItemFragment(
                R.string.tips_breeding,
                arrayOf(R.mipmap.breeding_diagram_foreground),
                false,
                emptyMap(),
                R.string.tips_breeding_desc
            )
            6 -> ExpandedItemFragment(
                R.string.examples_of_breeding_areas,
                arrayOf(
                    R.mipmap.bird_bath_foreground,
                    R.mipmap.ornamental_pond_foreground,
                    R.mipmap.inflatable_pool_foreground,
                    R.mipmap.kids_toys_foreground,
                    R.mipmap.ornaments_foreground,
                    R.mipmap.plant_pots_foreground,
                    R.mipmap.puddle_container_foreground,
                    R.mipmap.uncleaned_gutters_foreground,
                    R.mipmap.uncleaned_drainages_foreground,
                    R.mipmap.yard_equipment_foreground
                ),
                false,
                mapOf(-1 to R.array.examples_of_breeding_areas_list),
                -1
            )
            else -> ExpandedItemFragment(
                R.string.what_can_you_do,
                emptyArray(),
                false,
                mapOf(
                    R.string.prevent_mosquitoes_breeding to R.array.prevent_mosquitoes_breeding_list,
                    R.string.avoid_bites to R.array.avoid_bites_list
                ),
                -1
            )
        }
    }

}