package com.example.mosquitotracker

import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerSupportFragment


class ExpandedItemFragment(

    private val mainTitleId: Int,
    private val imageIds: Array<Int>,
    private val hasVideo: Boolean,
    private val listTextMap: Map<Int, Int>,
    private val descTextId:
    Int
) : Fragment(), YouTubePlayer.OnInitializedListener {
    private lateinit var closeButton: ImageView
    private lateinit var mainTitleTextView: TextView
    private lateinit var mosquitoVideo: YouTubePlayerSupportFragment
    private lateinit var youTubeLayoutContainer: LinearLayout
    private lateinit var mosquitoImageViewPager: ViewPager
    private lateinit var listRecyclerView: RecyclerView
    private lateinit var descText: TextView
    private lateinit var youTubePlayer: YouTubePlayer
    private lateinit var viewPagerIndicator: TabLayout
    private var isFullScreen: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.expanded_item, container, false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeButton = view.findViewById(R.id.close_button)
        val mainActivity = activity as MainActivity
        closeButton.setOnClickListener {
            mainActivity.closeExpandedFragment(this)
        }
        initializeLayout()
    }

    private fun initializeLayout() {
        youTubeLayoutContainer = view!!.findViewById(R.id.youtube_layout_container)
        listRecyclerView = view!!.findViewById(R.id.list_text_recycler_view)
        mainTitleTextView = view!!.findViewById(R.id.main_title)
        mainTitleTextView.text = getString(mainTitleId)
        mosquitoImageViewPager = view!!.findViewById(R.id.mosquito_image_view_pager)
        viewPagerIndicator = view!!.findViewById(R.id.view_pager_indicator)
        descText = view!!.findViewById(R.id.description_text)
        setAttributes()
    }

    private fun setAttributes() {
        if (imageIds.isNotEmpty()) {
            mosquitoImageViewPager.visibility = View.VISIBLE
            mosquitoImageViewPager.adapter = MosquitoImageViewPager(context!!, imageIds)
            if (imageIds.size > 1) {
                viewPagerIndicator.visibility = View.VISIBLE
                viewPagerIndicator.setupWithViewPager(mosquitoImageViewPager)
            }
        }
        if (hasVideo) {
            youTubeLayoutContainer.visibility = View.VISIBLE
            mosquitoVideo =
                childFragmentManager.findFragmentById(R.id.youtube_player_fragment) as YouTubePlayerSupportFragment
            mosquitoVideo.initialize(getString(R.string.google_api_key), this)
        }
        if (listTextMap.isNotEmpty()) {
            val listTextAdapter =
                ListTextAdapter(
                    listTextMap.keys.toList(),
                    listTextMap.values.toList(),
                    resources
                )
            listRecyclerView.visibility = View.VISIBLE
            listRecyclerView.isFocusable = false
            listRecyclerView.layoutManager = LinearLayoutManager(context!!)
            listRecyclerView.adapter = listTextAdapter
        }
        if (descTextId != -1) {
            descText.visibility = View.VISIBLE
            descText.movementMethod = LinkMovementMethod.getInstance()
            descText.text = Html.fromHtml(getString(descTextId), HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
    }

    fun isFullScreen(): Boolean {
        return isFullScreen
    }

    fun closeFullScreen() {
        youTubePlayer.setFullscreen(false)
    }

    override fun onInitializationSuccess(
        provider: YouTubePlayer.Provider,
        player: YouTubePlayer,
        b: Boolean
    ) {
        if (!b) {
            youTubePlayer = player
            youTubePlayer.cueVideo(getString(R.string.mosquito_video_id), 1)
            youTubePlayer.setOnFullscreenListener { fullScreen ->
                isFullScreen = fullScreen
                if (isFullScreen) {
                    mainTitleTextView.visibility = View.GONE
                    descText.visibility = View.GONE
                    closeButton.visibility = View.GONE
                } else {
                    mainTitleTextView.visibility = View.VISIBLE
                    descText.visibility = View.VISIBLE
                    closeButton.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onInitializationFailure(
        provider: YouTubePlayer.Provider,
        youTubeInitializationResult: YouTubeInitializationResult
    ) {
        Toast.makeText(activity, "Youtube video failed to play", Toast.LENGTH_SHORT).show()
    }
}
