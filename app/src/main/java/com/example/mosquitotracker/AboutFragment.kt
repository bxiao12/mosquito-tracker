package com.example.mosquitotracker

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.button.MaterialButton

class AboutFragment : Fragment() {
    private lateinit var closeButton: ImageView
    private lateinit var callButton: MaterialButton
    private lateinit var webButton: MaterialButton
    private lateinit var emailButton: MaterialButton
    private lateinit var mainActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.about_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as MainActivity
        closeButton = view.findViewById(R.id.close_button_about)
        callButton = view.findViewById(R.id.call_button)
        webButton = view.findViewById(R.id.visit_website_button)
        emailButton = view.findViewById(R.id.email_button)
        closeButton.setOnClickListener {
            mainActivity.closeSettingsFragment()
        }
        callButton.setOnClickListener {
            dialPhone()
        }
        webButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.mosquito_url)))
            startActivity(intent)
        }
        emailButton.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_SENDTO,
                Uri.fromParts("mailto", getString(R.string.email_address), null)
            )
            startActivity(intent)
        }
    }

    private fun dialPhone() {
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.CALL_PHONE) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.CALL_PHONE),
                REQUEST_CALL_PHONE
            )
        } else {
            val intent =
                Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.telephone_number)))
            startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CALL_PHONE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dialPhone()
                }
                return
            }
        }
    }


    companion object {
        private const val REQUEST_CALL_PHONE = 1
    }

}